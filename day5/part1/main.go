package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		largest int64 = 0
	)

	for _, line := range lines {
		if line == "" {
			continue
		}

		var (
			rowStr  string
			seatStr string
			row     int64
			seat    int64
			seatID  int64
			err     error
		)

		fmt.Println(line)
		rowStr = line[0:7]
		seatStr = line[7:10]

		rowStr = strings.ReplaceAll(rowStr, "F", "0")
		rowStr = strings.ReplaceAll(rowStr, "B", "1")
		row, err = strconv.ParseInt(rowStr, 2, 64)
		if err != nil {
			fmt.Printf("%s doesn't turn into a row %s\n", line, rowStr)
			continue
		}

		seatStr = strings.ReplaceAll(seatStr, "R", "1")
		seatStr = strings.ReplaceAll(seatStr, "L", "0")
		seat, err = strconv.ParseInt(seatStr, 2, 64)
		if err != nil {
			fmt.Printf("%s doesn't turn into a seat %s\n", line, seatStr)
			continue
		}

		seatID = row*8 + seat
		fmt.Println(seatID)

		if seatID > largest {
			largest = seatID
		}
	}

	fmt.Printf("Largest seat ID: %d\n", largest)
}
