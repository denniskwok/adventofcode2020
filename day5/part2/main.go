package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		seatIDs []int64 = make([]int64, 0, len(lines))
	)

	for _, line := range lines {
		if line == "" {
			continue
		}

		var (
			rowStr  string
			seatStr string
			row     int64
			seat    int64
			seatID  int64
			err     error
		)

		fmt.Println(line)
		rowStr = line[0:7]
		seatStr = line[7:10]

		rowStr = strings.ReplaceAll(rowStr, "F", "0")
		rowStr = strings.ReplaceAll(rowStr, "B", "1")
		row, err = strconv.ParseInt(rowStr, 2, 64)
		if err != nil {
			fmt.Printf("%s doesn't turn into a row %s\n", line, rowStr)
			continue
		}

		seatStr = strings.ReplaceAll(seatStr, "R", "1")
		seatStr = strings.ReplaceAll(seatStr, "L", "0")
		seat, err = strconv.ParseInt(seatStr, 2, 64)
		if err != nil {
			fmt.Printf("%s doesn't turn into a seat %s\n", line, seatStr)
			continue
		}

		seatID = row*8 + seat
		fmt.Println(seatID)
		seatIDs = append(seatIDs, seatID)
	}

	sort.Slice(seatIDs, func(i, j int) bool { return seatIDs[i] < seatIDs[j] })
	fmt.Println(seatIDs)

	// find the hole
	for i, seatID := range seatIDs {
		if i == len(seatIDs)-1 {
			break
		}

		if seatID+1 == seatIDs[i+1] {
			continue
		}

		fmt.Printf("My seat is %d\n", seatID+1)
	}
}
