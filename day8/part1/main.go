package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		accumulator int = 0
		pc          int = 0
		visited     []int
	)

	visited = make([]int, 0, len(visited))
	for i := 0; i < len(lines); i++ {
		visited = append(visited, 0)
	}

	for {
		var (
			instruction string
			val         int
		)

		visited[pc]++

		if visited[pc] >= 2 {
			break
		}

		instruction, val = parseInstruction(lines[pc])
		fmt.Printf("%03d: %s %d accum: %d\n", pc, instruction, val, accumulator)
		switch instruction {
		case "nop":
			pc++
		case "acc":
			accumulator += val
			pc++
		case "jmp":
			pc += val
		}
	}

	fmt.Printf("accum: %d\n", accumulator)
}

func parseInstruction(line string) (string, int) {
	var (
		pieces      []string
		instruction string
		val         int
		err         error
	)

	pieces = strings.Split(line, " ")
	if len(pieces) != 2 {
		fmt.Printf("line %s did not parse\n", line)
		return "", 0
	}

	instruction = pieces[0]
	val, err = strconv.Atoi(pieces[1])
	if err != nil {
		fmt.Println(err)
		return "", 0
	}

	return instruction, val
}
