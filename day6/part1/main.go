package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		answered map[string]bool = map[string]bool{}
		total    int
	)

	for _, line := range lines {
		if line == "" {
			// new set of questions
			total += len(answered)
			fmt.Println(answered)
			answered = map[string]bool{}
			continue
		}

		for i := 0; i < len(line); i++ {
			var char string = line[i : i+1]
			answered[char] = true
		}
	}

	// last one
	// new set of questions
	total += len(answered)
	fmt.Println(answered)
	answered = map[string]bool{}

	fmt.Println(total)
}
