package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		answered map[string]int = map[string]int{}
		numForms int
		total    int
	)

	for _, line := range lines {
		if line == "" {
			// new set of questions

			for _, foo := range answered {
				if foo == numForms {
					total++
				}
			}
			// total += len(answered)
			fmt.Println(answered)
			answered = map[string]int{}
			numForms = 0
			continue
		}

		numForms++
		for i := 0; i < len(line); i++ {
			var (
				char  string = line[i : i+1]
				found bool
			)
			_, found = answered[char]
			if !found {
				answered[char] = 0
			}
			answered[char]++
		}
	}

	// last one
	for _, foo := range answered {
		if foo == numForms {
			total++
		}
	}
	// total += len(answered)
	fmt.Println(answered)
	answered = map[string]int{}

	fmt.Println(total)
}
