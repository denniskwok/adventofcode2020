package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"regexp"
	"sort"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		contains map[string][]string
		solved   map[string]bool
		found    int
	)

	contains = buildContains(lines)
	solved = make(map[string]bool, len(contains))

	found = 0
	for bagColor := range contains {
		foundAlready, ok := solved[bagColor]
		if ok {
			if foundAlready {
				found++
			}
			continue
		}

		fmt.Printf("Starting recurse on %s\n", bagColor)
		if recursiveSearch(bagColor, contains, solved) {
			solved[bagColor] = true
			found++
		} else {
			solved[bagColor] = false
		}
	}

	fmt.Printf("%+v\n", solved)
	fmt.Println(found)
}

func recursiveSearch(bagColor string, contains map[string][]string, solved map[string]bool) bool {
	var (
		foundAlready  bool
		recurseResult bool
		ok            bool
	)

	foundAlready, ok = solved[bagColor]
	if ok {
		return foundAlready
	}

	for _, containedColor := range contains[bagColor] {
		if containedColor == "shiny gold" {
			solved[bagColor] = true
			return true
		}

		fmt.Printf("As part of %s, looking at %s\n", bagColor, containedColor)
		foundAlready, ok := solved[bagColor]
		if ok && foundAlready {
			solved[bagColor] = true
			return true
		}

		recurseResult = recursiveSearch(containedColor, contains, solved)
		if recurseResult {
			solved[bagColor] = true
			return true
		}
	}

	solved[bagColor] = false
	return false
}

func buildContains(lines []string) map[string][]string {
	var (
		contains map[string][]string
		// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
		// faded blue bags contain no other bags.
		lineParser = regexp.MustCompile(`(.*) bags contain (.*)\.`)
		// 3 faded blue bags, 4 dotted black bags
		bagCountParser = regexp.MustCompile(`(\d)+ (.*) bags?`)
	)

	contains = make(map[string][]string, len(lines))

	// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
	// faded blue bags contain no other bags.
	// parse
	for _, line := range lines {
		if line == "" {
			continue
		}

		var (
			matches  []string
			pieces   []string
			holds    []string
			bagColor string
		)

		matches = lineParser.FindStringSubmatch(line)
		if len(matches) != 3 {
			fmt.Printf("%s did not parse\n", line)
			continue
		}

		bagColor = matches[1]

		pieces = strings.Split(matches[2], ", ")
		// fmt.Println(bagColor)
		// fmt.Println(pieces)

		holds = make([]string, 0, len(pieces))
		for _, piece := range pieces {
			if piece == "no other bags" {
				continue
			}

			matches = bagCountParser.FindStringSubmatch(piece)
			if len(matches) != 3 {
				fmt.Printf("%s did not parse as a bagCount\n", piece)
				continue
			}

			// count matches[1]
			holds = append(holds, matches[2])
		}

		sort.Strings(holds)
		contains[bagColor] = holds
	}

	// fmt.Printf("%+v\n", contains)
	return contains
}
