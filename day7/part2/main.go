package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"regexp"
	// "sort"
	"strconv"
	"strings"
)

type BagCount struct {
	BagColor string
	Count    int
}

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		contains map[string][]BagCount
		bagCount int
	)

	contains = buildContains(lines)
	fmt.Printf("%+v\n", contains)

	bagCount = recursiveCount("shiny gold", contains)
	fmt.Println(bagCount - 1)
}

func recursiveCount(bagColor string, contains map[string][]BagCount) int {
	var bagCount int = 1

	for _, bc := range contains[bagColor] {
		bagCount += bc.Count * recursiveCount(bc.BagColor, contains)
	}

	return bagCount
}

func buildContains(lines []string) map[string][]BagCount {
	var (
		contains map[string][]BagCount
		// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
		// faded blue bags contain no other bags.
		lineParser = regexp.MustCompile(`(.*) bags contain (.*)\.`)
		// 3 faded blue bags, 4 dotted black bags
		bagCountParser = regexp.MustCompile(`(\d)+ (.*) bags?`)
	)

	contains = make(map[string][]BagCount, len(lines))

	// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
	// faded blue bags contain no other bags.
	// parse
	for _, line := range lines {
		if line == "" {
			continue
		}

		var (
			matches  []string
			pieces   []string
			holds    []BagCount
			bagColor string
		)

		matches = lineParser.FindStringSubmatch(line)
		if len(matches) != 3 {
			fmt.Printf("%s did not parse\n", line)
			continue
		}

		bagColor = matches[1]

		pieces = strings.Split(matches[2], ", ")
		// fmt.Println(bagColor)
		// fmt.Println(pieces)

		holds = make([]BagCount, 0, len(pieces))
		for _, piece := range pieces {
			if piece == "no other bags" {
				continue
			}

			matches = bagCountParser.FindStringSubmatch(piece)
			if len(matches) != 3 {
				fmt.Printf("%s did not parse as a bagCount\n", piece)
				continue
			}

			var (
				bc    BagCount
				count int
				err   error
			)
			count, err = strconv.Atoi(matches[1])
			if err != nil {
				fmt.Printf("%s did not int parse\n", matches[1])
				continue
			}

			bc.BagColor = matches[2]
			bc.Count = count

			// count matches[1]
			holds = append(holds, bc)
		}

		contains[bagColor] = holds
	}

	// fmt.Printf("%+v\n", contains)
	return contains
}
