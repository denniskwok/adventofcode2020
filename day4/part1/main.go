package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

type Passport struct {
	byr string // (Birth Year)
	iyr string // (Issue Year)
	eyr string // (Expiration Year)
	hgt string // (Height)
	hcl string // (Hair Color)
	ecl string // (Eye Color)
	pid string // (Passport ID)
	cid string // (Country ID)
}

func (p *Passport) Valid() bool {
	if p.byr == "" || p.iyr == "" || p.eyr == "" || p.hgt == "" || p.hcl == "" || p.ecl == "" ||
		p.pid == "" {
		return false
	}
	return true
}

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		passports []Passport
		current   Passport
		valid     int
	)

	for _, line := range lines {
		var pairs []string
		if line == "" {
			passports = append(passports, current)
			current = Passport{}
		}

		pairs = strings.Split(line, " ")
		if len(pairs) <= 0 {
			fmt.Printf("no pairs in %s\n", line)
			continue
		}

		for _, pair := range pairs {
			var (
				pairParts []string
				key       string
				value     string
			)

			pairParts = strings.Split(pair, ":")
			if len(pairParts) != 2 {
				fmt.Printf("%s is not a proper pair\n", pair)
				continue
			}

			key, value = pairParts[0], pairParts[1]
			switch key {
			case "byr":
				current.byr = value
			case "iyr":
				current.iyr = value
			case "eyr":
				current.eyr = value
			case "hgt":
				current.hgt = value
			case "hcl":
				current.hcl = value
			case "ecl":
				current.ecl = value
			case "pid":
				current.pid = value
			case "cid":
				current.cid = value
			default:
				fmt.Printf("Pair %s is invalid\n", pair)
			}
		}
	}

	passports = append(passports, current)

	for _, passport := range passports {
		fmt.Printf("%+v\n", passport)
	}

	for _, passport := range passports {
		if passport.Valid() {
			valid++
		}
	}

	fmt.Println(valid)
}
