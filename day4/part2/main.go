package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type Passport struct {
	byr string // (Birth Year)
	iyr string // (Issue Year)
	eyr string // (Expiration Year)
	hgt string // (Height)
	hcl string // (Hair Color)
	ecl string // (Eye Color)
	pid string // (Passport ID)
	cid string // (Country ID)
}

var hairColorRegex = regexp.MustCompile(`#([0-9]|[a-f]){6}`)

func (p *Passport) Valid() bool {
	var (
		tempInt int
		tempStr string
		err     error
	)

	// fmt.Printf("byr %s\n", p.byr)
	tempInt, err = strconv.Atoi(p.byr)
	if err != nil || tempInt < 1920 || 2002 < tempInt {
		return false
	}

	tempInt, err = strconv.Atoi(p.iyr)
	if err != nil || tempInt < 2010 || 2020 < tempInt {
		return false
	}

	tempInt, err = strconv.Atoi(p.eyr)
	if err != nil || tempInt < 2020 || 2030 < tempInt {
		return false
	}

	if len(p.hgt) < 3 {
		return false
	}
	tempStr = p.hgt[0 : len(p.hgt)-2]
	switch {
	case strings.HasSuffix(p.hgt, "in"):
		tempInt, err = strconv.Atoi(tempStr)
		if err != nil || tempInt < 59 || 76 < tempInt {
			return false
		}
	case strings.HasSuffix(p.hgt, "cm"):
		tempInt, err = strconv.Atoi(tempStr)
		if err != nil || tempInt < 150 || 193 < tempInt {
			return false
		}
	default:
		return false
	}

	if !hairColorRegex.MatchString(p.hcl) {
		return false
	}

	switch p.ecl {
	case "amb":
	case "blu":
	case "brn":
	case "gry":
	case "grn":
	case "hzl":
	case "oth":
	default:
		return false
	}

	if len(p.pid) != 9 {
		return false
	}

	_, err = strconv.Atoi(p.pid)
	if err != nil {
		return false
	}

	return true
}

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		passports []Passport
		current   Passport
		valid     int
	)

	for _, line := range lines {
		var pairs []string
		if line == "" {
			passports = append(passports, current)
			current = Passport{}
		}

		pairs = strings.Split(line, " ")
		if len(pairs) <= 0 {
			fmt.Printf("no pairs in %s\n", line)
			continue
		}

		for _, pair := range pairs {
			var (
				pairParts []string
				key       string
				value     string
			)

			pairParts = strings.Split(pair, ":")
			if len(pairParts) != 2 {
				fmt.Printf("%s is not a proper pair\n", pair)
				continue
			}

			key, value = pairParts[0], pairParts[1]
			switch key {
			case "byr":
				current.byr = value
			case "iyr":
				current.iyr = value
			case "eyr":
				current.eyr = value
			case "hgt":
				current.hgt = value
			case "hcl":
				current.hcl = value
			case "ecl":
				current.ecl = value
			case "pid":
				current.pid = value
			case "cid":
				current.cid = value
			default:
				fmt.Printf("Pair %s is invalid\n", pair)
			}
		}
	}

	passports = append(passports, current)

	// for _, passport := range passports {
	// 	fmt.Printf("%+v\n", passport)
	// }

	for _, passport := range passports {
		if passport.Valid() {
			fmt.Printf("%+v\n", passport)
			valid++
		}
	}

	fmt.Println(valid)
}
