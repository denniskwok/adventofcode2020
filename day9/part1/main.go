package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputInts  []int
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	inputInts = make([]int, 0, len(inputLines))

	for _, line := range inputLines {
		if line == "" {
			break
		}

		parsed, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err)
			continue
		}
		inputInts = append(inputInts, parsed)
	}

	fmt.Println(inputInts)

	solve(inputInts)
}

func solve(ints []int) {
	const (
		window int = 25
	)

	var (
		i int
	)

	for i = 0; i < len(ints)-window; i++ {
		if !isNextValid(ints, i, i+window) {
			fmt.Printf("%d does not follow the rule\n", ints[i+window])
		}
	}
}

func isNextValid(ints []int, startIdx int, endIdx int) bool {
	if endIdx >= len(ints) {
		return false
	}

	var (
		i    int
		j    int
		next int
	)

	next = ints[endIdx]

	for i = startIdx; i < endIdx; i++ {
		for j = i + 1; j < endIdx; j++ {
			var sum int
			sum = ints[i] + ints[j]
			// fmt.Printf("%d + %d = %d against %d\n", ints[i], ints[j], sum, next)
			if sum == next {
				return true
			}
		}
	}
	return false
}
