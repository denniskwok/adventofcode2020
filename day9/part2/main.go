package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputInts  []int
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	inputInts = make([]int, 0, len(inputLines))

	for _, line := range inputLines {
		if line == "" {
			break
		}

		parsed, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err)
			continue
		}
		inputInts = append(inputInts, parsed)
	}

	fmt.Println(len(inputInts))
	fmt.Println(inputInts)

	solve(inputInts)
}

func solve(ints []int) {
	const (
		window int = 25
	)

	var (
		i        int
		leftIdx  int
		rightIdx int
		max      int
		min      int
		sum      int
		target   int
	)

	for i = 0; i < len(ints)-window; i++ {
		if !isNextValid(ints, i, i+window) {
			target = ints[i+window]
			fmt.Printf("%d does not follow the rule\n", target)
			break
		}
	}

	leftIdx = 0
	rightIdx = 0
	sum = ints[0]
	for sum != target {
		if sum < target {
			// add right
			rightIdx++
			sum += ints[rightIdx]
		} else if sum > target {
			// increment left, subtract old
			sum -= ints[leftIdx]
			leftIdx++
		}
	}

	fmt.Printf("left: %d right: %d\n", leftIdx, rightIdx)
	max, min = findMinMax(ints, leftIdx, rightIdx+1)
	fmt.Printf("max: %d min: %d sum: %d\n", max, min, max+min)
}

func isNextValid(ints []int, startIdx int, endIdx int) bool {
	if endIdx >= len(ints) {
		return false
	}

	var (
		i    int
		j    int
		next int
	)

	next = ints[endIdx]

	for i = startIdx; i < endIdx; i++ {
		for j = i + 1; j < endIdx; j++ {
			var sum int
			sum = ints[i] + ints[j]
			// fmt.Printf("%d + %d = %d against %d\n", ints[i], ints[j], sum, next)
			if sum == next {
				return true
			}
		}
	}
	return false
}

func findMinMax(ints []int, startIdx int, endIdx int) (int, int) {
	var (
		i   int
		max int
		min int
	)

	max = ints[startIdx]
	min = ints[startIdx]

	for i = startIdx; i < endIdx; i++ {
		val := ints[i]

		if val > max {
			max = val
		}

		if val < min {
			min = val
		}
	}

	return max, min
}
