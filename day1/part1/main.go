package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputInts  []int
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	inputInts = make([]int, 0, len(inputLines))

	for _, line := range inputLines {
		if line == "" {
			break
		}

		parsed, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err)
			continue
		}
		inputInts = append(inputInts, parsed)
	}

	fmt.Println(inputInts)

	solve(inputInts)
}

func solve(ints []int) {
	sort.Ints(ints)

	for i := 0; i < len(ints); i++ {
		for j := i + 1; j < len(ints); j++ {
			sum := ints[i] + ints[j]

			if sum == 2020 {
				fmt.Println(ints[i] * ints[j])
			}
		}
	}
}
