package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputInts  []int64
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	inputInts = make([]int64, 0, len(inputLines))

	for _, line := range inputLines {
		if line == "" {
			break
		}

		parsed, err := strconv.ParseInt(line, 10, 64)
		if err != nil {
			fmt.Println(err)
			continue
		}
		inputInts = append(inputInts, parsed)
	}

	inputInts = append(inputInts, 0)
	fmt.Println(inputInts)

	solve(inputInts)
}

func solve(ints []int64) {
	var (
		paths map[int64]int64

		i int
	)

	sort.Slice(ints, func(i, j int) bool { return ints[i] < ints[j] })
	fmt.Println(ints)

	paths = make(map[int64]int64, len(ints))
	paths[ints[len(ints)-1]] = 1

	for i = len(ints) - 2; i >= 0; i-- {
		calcNumPaths(i, ints, paths)
	}

	fmt.Printf("%+v\n", paths)
	fmt.Printf("%d\n", paths[ints[0]])
}

func calcNumPaths(index int, ints []int64, paths map[int64]int64) {
	var (
		sum   int64
		value int64

		i int
	)

	value = ints[index]

	for i = 1; i < 4; i++ {
		var (
			difference   int64
			forwardValue int64
			pathCount    int64
			forwardIndex int
			found        bool
		)

		forwardIndex = index + i
		if forwardIndex >= len(ints) {
			break
		}

		forwardValue = ints[forwardIndex]
		difference = forwardValue - value
		if difference > 3 {
			continue
		}

		pathCount, found = paths[forwardValue]
		if !found {
			fmt.Printf("Should have found %d\n")
			return
		}

		sum += pathCount
	}

	paths[value] = sum
}
