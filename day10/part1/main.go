package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputInts  []int
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	inputInts = make([]int, 0, len(inputLines))

	for _, line := range inputLines {
		if line == "" {
			break
		}

		parsed, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err)
			continue
		}
		inputInts = append(inputInts, parsed)
	}

	fmt.Println(inputInts)

	solve(inputInts)
}

func solve(ints []int) {
	var (
		currentJoltage int
		// finalJoltage     int
		oneDifferences   int
		threeDifferences int
	)

	sort.Ints(ints)
	fmt.Println(ints)

	currentJoltage = 0
	// finalJoltage = ints[len(ints)-1]
	oneDifferences = 0
	threeDifferences = 0

	for _, outputJoltage := range ints {
		var difference int
		difference = outputJoltage - currentJoltage

		fmt.Printf("difference %d\n", difference)

		switch difference {
		case 1:
			oneDifferences++
		case 3:
			threeDifferences++
		}
		currentJoltage = outputJoltage
	}
	threeDifferences++

	fmt.Printf("1 diff: %d\n", oneDifferences)
	fmt.Printf("3 diff: %d\n", threeDifferences)
	fmt.Printf("product: %d\n", oneDifferences*threeDifferences)
}
