package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	north   string = "N" // means to move north by the given value.
	south   string = "S" // means to move south by the given value.
	east    string = "E" // means to move east by the given value.
	west    string = "W" // means to move west by the given value.
	left    string = "L" // means to turn left the given number of degrees.
	right   string = "R" // means to turn right the given number of degrees.
	forward string = "F" // means to move forward by the given value in the direction the ship is currently facing.
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		x         int
		y         int
		waypointX int
		waypointY int
	)

	waypointX = 10
	waypointY = -1

	for _, line := range input {
		var (
			instruction string
			steps       int

			err error
		)

		instruction = line[0:1]
		steps, err = strconv.Atoi(line[1:len(line)])
		if err != nil {
			fmt.Println(line)
			fmt.Println(err)
			continue
		}

		fmt.Printf("x: %d y: %d wayX: %d wayY: %d inst: %s\n", x, y, waypointX, waypointY, line)

		switch instruction {
		case north:
			waypointY -= steps
		case south:
			waypointY += steps
		case east:
			waypointX += steps
		case west:
			waypointX -= steps
		case left:
			for steps > 0 {
				waypointX, waypointY = waypointY, -waypointX
				steps -= 90
			}
		case right:
			for steps > 0 {
				waypointX, waypointY = -waypointY, waypointX
				steps -= 90
			}
		case forward:
			x += waypointX * steps
			y += waypointY * steps
		}
	}

	if x < 0 {
		x = -x
	}

	if y < 0 {
		y = -y
	}

	fmt.Printf("x: %d y: %d md: %d\n", x, y, x+y)
}
