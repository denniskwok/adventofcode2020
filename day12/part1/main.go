package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	north   string = "N" // means to move north by the given value.
	south   string = "S" // means to move south by the given value.
	east    string = "E" // means to move east by the given value.
	west    string = "W" // means to move west by the given value.
	left    string = "L" // means to turn left the given number of degrees.
	right   string = "R" // means to turn right the given number of degrees.
	forward string = "F" // means to move forward by the given value in the direction the ship is currently facing.
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		heading string
		x       int
		y       int
	)

	heading = east

	for _, line := range input {
		var (
			instruction string
			steps       int

			err error
		)

		instruction = line[0:1]
		steps, err = strconv.Atoi(line[1:len(line)])
		if err != nil {
			fmt.Println(line)
			fmt.Println(err)
			continue
		}

		fmt.Printf("x: %d y: %d heading: %s inst: %s\n", x, y, heading, line)

		switch instruction {
		case north:
			y -= steps

		case south:
			y += steps
		case east:
			x += steps
		case west:
			x -= steps
		case left:
			for steps > 0 {
				switch heading {
				case north:
					heading = west
				case south:
					heading = east
				case east:
					heading = north
				case west:
					heading = south
				}
				steps -= 90
			}
		case right:
			for steps > 0 {
				switch heading {
				case north:
					heading = east
				case south:
					heading = west
				case east:
					heading = south
				case west:
					heading = north
				}
				steps -= 90
			}
		case forward:
			switch heading {
			case north:
				y -= steps
			case south:
				y += steps
			case east:
				x += steps
			case west:
				x -= steps
			}
		}
	}

	if x < 0 {
		x = -x
	}

	if y < 0 {
		y = -y
	}

	fmt.Printf("x: %d y: %d md: %d\n", x, y, x+y)
}
