package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(lines []string) {
	var (
		valid int = 0
	)

	for _, line := range lines {
		if isValid(line) {
			fmt.Printf("%s is valid\n", line)
			valid++
		} else {
			fmt.Printf("%s is invalid\n", line)
		}
	}

	fmt.Println(valid)
}

func isValid(line string) bool {
	var (
		pieces []string
		err    error

		input        string
		maxOccurence int
		minOccurence int
		neededChar   string
		occurences   int
	)

	pieces = strings.Split(line, " ")
	if len(pieces) != 3 {
		fmt.Printf("%s did not parse properly\n", line)
		return false
	}

	input = pieces[2]
	neededChar = pieces[1][0:1]

	pieces = strings.Split(pieces[0], "-")
	if len(pieces) != 2 {
		fmt.Printf("%s did not split into two pieces\n", pieces)
		return false
	}

	minOccurence, err = strconv.Atoi(pieces[0])
	if err != nil {
		fmt.Printf("%s it not an int\n", pieces[0])
		return false
	}

	maxOccurence, err = strconv.Atoi(pieces[1])
	if err != nil {
		fmt.Printf("%s it not an int\n", pieces[1])
		return false
	}

	occurences = strings.Count(input, neededChar)
	return minOccurence <= occurences && occurences <= maxOccurence
}
