package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
		result     int = 1
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)
	// 	Right 1, down 1.
	// Right 3, down 1. (This is the slope you already checked.)
	// Right 5, down 1.
	// Right 7, down 1.
	// Right 1, down 2.
	result *= solve(inputLines, 1, 1)
	result *= solve(inputLines, 3, 1)
	result *= solve(inputLines, 5, 1)
	result *= solve(inputLines, 7, 1)
	result *= solve(inputLines, 1, 2)
	fmt.Printf("Result: %d\n", result)
}

func solve(treeMap []string, xAdvance int, yAdvance int) int {
	const (
		open string = "."
		tree string = "#"
	)

	var (
		x         int = 0
		y         int = 0
		treeCount int = 0
	)

	for y = 0; y < len(treeMap); y += yAdvance {
		if treeMap[y][x:x+1] == tree && y != 0 {
			fmt.Printf("space %s %d, %d has a tree\n", treeMap[y][x:x+1], x, y)
			treeCount++
		} else {
			fmt.Printf("space %s %d, %d is not a tree\n", treeMap[y][x:x+1], x, y)
		}
		x += xAdvance
		x %= len(treeMap[y])
	}

	fmt.Println(treeCount)
	return treeCount
}
