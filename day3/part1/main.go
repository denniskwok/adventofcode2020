package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(treeMap []string) {
	const (
		open string = "."
		tree string = "#"
	)

	var (
		x         int = 3
		y         int = 1
		treeCount int = 0
	)

	for y = 1; y < len(treeMap); y++ {
		if treeMap[y][x:x+1] == tree {
			fmt.Printf("space %s %d, %d has a tree\n", treeMap[y][x:x+1], x, y)
			treeCount++
		} else {
			fmt.Printf("space %s %d, %d is not a tree\n", treeMap[y][x:x+1], x, y)
		}
		x += 3
		x %= len(treeMap[y])
	}

	fmt.Println(treeCount)
}
