package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part1 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		oneMask  int64
		zeroMask int64

		memory map[int64]int64
		sum    int64

		err error
	)

	memory = make(map[int64]int64, len(input))
	for _, line := range input {

		switch {
		case strings.Contains(line, "mask"):

			line = strings.Replace(line, "mask = ", "", 1)

			oneMask, err = strconv.ParseInt(strings.ReplaceAll(line, "X", "0"), 2, 64)
			if err != nil {
				fmt.Println(err)
				continue
			}

			fmt.Printf("one mask: %036b\n", oneMask)

			zeroMask, err = strconv.ParseInt(strings.ReplaceAll(line, "X", "1"), 2, 64)
			if err != nil {
				fmt.Println(err)
				continue
			}

			fmt.Printf("zero mask: %036b\n", zeroMask)
		default:
			var (
				address   int64
				origValue int64
				value     int64

				splitParts []string
			)
			line = strings.Replace(line, "mem[", "", 1)
			line = strings.Replace(line, "] = ", " ", 1)

			splitParts = strings.Split(line, " ")
			address, err = strconv.ParseInt(splitParts[0], 10, 64)
			if err != nil {
				fmt.Println(err)
				continue
			}

			value, err = strconv.ParseInt(splitParts[1], 10, 64)
			if err != nil {
				fmt.Println(err)
				continue
			}

			origValue = value
			value |= oneMask
			value &= zeroMask

			fmt.Printf("address %d orig %d new %d\n", address, origValue, value)
			memory[address] = value
		}
	}

	for _, value := range memory {
		sum += value
	}

	fmt.Printf("sum: %d\n", sum)

}
