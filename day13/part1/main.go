package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		nearestTime  map[int]int
		busIDs       []int
		shortestBus  int
		shortestTime int
		startTime    int

		pieces []string
		err    error
	)

	nearestTime = make(map[int]int)
	startTime, err = strconv.Atoi(input[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	pieces = strings.Split(input[1], ",")
	busIDs = make([]int, 0, len(pieces))

	for _, piece := range pieces {
		if piece == "x" {
			continue
		}

		tempInt, err := strconv.Atoi(piece)
		if err != nil {
			fmt.Println(err)
			continue
		}

		busIDs = append(busIDs, tempInt)
	}

	fmt.Printf("start time: %d\n", startTime)
	fmt.Printf("buses: %v\n", busIDs)

	shortestBus = -1
	shortestTime = -1
	for _, busID := range busIDs {
		nearestMultiple := (startTime / busID) * busID

		if nearestMultiple == startTime {
			nearestTime[busID] = startTime

		} else {
			nearestTime[busID] = nearestMultiple + busID
		}

		if shortestTime == -1 || nearestTime[busID] < shortestTime {
			shortestBus = busID
			shortestTime = nearestTime[busID]
		}
	}

	fmt.Printf("nearest times: %v\n", nearestTime)
	fmt.Printf("busID: %d time: %d val: %d\n",
		shortestBus, shortestTime, (shortestTime-startTime)*shortestBus)
}
