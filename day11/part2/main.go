package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

const (
	openSeat     string = "L"
	occupiedSeat string = "#"
	floor        string = "."
)

type State []string

func (s State) Equal(s2 State) bool {
	var (
		i int
	)

	if len(s) != len(s2) {
		return false
	}

	for i = range s {
		if s[i] != s2[i] {
			return false
		}
	}
	return true
}

func (s State) Print() {
	for _, row := range s {
		fmt.Println(row)
	}
}

func (s State) NumOccupied() int {
	var sum int = 0
	for row := range s {
		var c int
		for c = 0; c < len(s[row]); c++ {
			if s[row][c:c+1] == occupiedSeat {
				sum++
			}
		}
	}
	return sum
}

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		newState State
		oldState State

		numRows int
		numCols int

		x    int
		y    int
		step int
	)

	oldState = make([]string, len(input))
	newState = input
	numRows = len(input)
	numCols = len(input[0])

	fmt.Println("Start")
	newState.Print()
	fmt.Println("=====")

	for !oldState.Equal(newState) {
		oldState = newState
		newState = make([]string, len(input))

		for y = 0; y < numRows; y++ {
			var newRow string

			for x = 0; x < numCols; x++ {
				var s string
				s = calcNextChar(oldState, x, y, numRows, numCols)
				newRow = newRow + s
			}
			newState[y] = newRow
		}

		fmt.Printf("Step %d\n", step)
		newState.Print()
		fmt.Println("=====")

		step++
	}

	fmt.Println(newState.NumOccupied())
}

func calcNextChar(input State, x int, y int, rows int, cols int) string {
	var (
		space string
	)

	space = input[y][x : x+1]

	switch space {
	case floor:
		return floor
	case openSeat:

		// fmt.Printf("%d, %d\n", x, y)
		for _, xStep := range []int{-1, 0, 1} {
			for _, yStep := range []int{-1, 0, 1} {
				if xStep == 0 && yStep == 0 {
					continue
				}

				if nextVisible(input, x, y, xStep, yStep, rows, cols) == occupiedSeat {
					return openSeat
				}
			}
		}

		return occupiedSeat
	case occupiedSeat:
		var numOccupied int

		for _, xStep := range []int{-1, 0, 1} {
			for _, yStep := range []int{-1, 0, 1} {
				if xStep == 0 && yStep == 0 {
					continue
				}

				if nextVisible(input, x, y, xStep, yStep, rows, cols) == occupiedSeat {
					numOccupied++
				}
			}
		}

		if numOccupied >= 5 {
			return openSeat
		}
		return occupiedSeat
	}

	return "X"
}

func nextVisible(input State, x int, y int, xStep int, yStep int, rows int, cols int) string {
	// start one step out
	x += xStep
	y += yStep

	for 0 <= x && x < cols && 0 <= y && y < rows {
		var space string = input[y][x : x+1]
		if space != floor {
			return space
		}

		x += xStep
		y += yStep
	}

	return floor
}
