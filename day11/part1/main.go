package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

const (
	openSeat     string = "L"
	occupiedSeat string = "#"
	floor        string = "."
)

type State []string

func (s State) Equal(s2 State) bool {
	var (
		i int
	)

	if len(s) != len(s2) {
		return false
	}

	for i = range s {
		if s[i] != s2[i] {
			return false
		}
	}
	return true
}

func (s State) Print() {
	for _, row := range s {
		fmt.Println(row)
	}
}

func (s State) NumOccupied() int {
	var sum int = 0
	for row := range s {
		var c int
		for c = 0; c < len(s[row]); c++ {
			if s[row][c:c+1] == occupiedSeat {
				sum++
			}
		}
	}
	return sum
}

func main() {
	var (
		args       []string
		inputBuf   []byte
		inputLines []string
		err        error
	)

	flag.Parse()
	args = flag.Args()

	if len(args) != 1 {
		fmt.Println("Usage: part2 <input path>")
		return
	}

	inputBuf, err = ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	inputLines = strings.Split(string(inputBuf), "\n")
	fmt.Println(inputLines)

	solve(inputLines)
}

func solve(input []string) {
	var (
		newState State
		oldState State

		numRows int
		numCols int

		x    int
		y    int
		step int
	)

	oldState = make([]string, len(input))
	newState = input
	numRows = len(input)
	numCols = len(input[0])

	fmt.Println("Start")
	newState.Print()
	fmt.Println("=====")

	for !oldState.Equal(newState) {
		oldState = newState
		newState = make([]string, len(input))

		for y = 0; y < numRows; y++ {
			var newRow string

			for x = 0; x < numCols; x++ {
				var s string
				s = calcNextChar(oldState, x, y, numRows, numCols)
				newRow = newRow + s
			}
			newState[y] = newRow
		}

		fmt.Printf("Step %d\n", step)
		newState.Print()
		fmt.Println("=====")

		step++
	}

	fmt.Println(newState.NumOccupied())
}

func calcNextChar(input State, x int, y int, rows int, cols int) string {
	var (
		space      string
		left       int
		right      int
		up         int
		down       int
		leftValid  bool
		rightValid bool
		upValid    bool
		downValid  bool
	)

	left = x - 1
	leftValid = (left >= 0)
	right = x + 1
	rightValid = (right < cols)
	up = y - 1
	upValid = (up >= 0)
	down = y + 1
	downValid = (down < rows)

	space = input[y][x : x+1]

	switch space {
	case floor:
		return floor
	case openSeat:

		// fmt.Printf("%d, %d\n", x, y)
		// check all 8 directions for non occupied
		if upValid && leftValid && input[up][left:left+1] == occupiedSeat {
			// fmt.Printf("up left occupied\n")
			return openSeat
		}

		if upValid && input[up][x:x+1] == occupiedSeat {
			// fmt.Printf("up occupied\n")
			return openSeat
		}

		if upValid && rightValid && input[up][right:right+1] == occupiedSeat {
			// fmt.Printf("up right occupied\n")
			return openSeat
		}

		if leftValid && input[y][left:left+1] == occupiedSeat {
			// fmt.Printf("left occupied\n")
			return openSeat
		}

		if rightValid && input[y][right:right+1] == occupiedSeat {
			// fmt.Printf("right occupied\n")
			return openSeat
		}

		if downValid && leftValid && input[down][left:left+1] == occupiedSeat {
			// fmt.Printf("down left occupied\n")
			return openSeat
		}

		if downValid && input[down][x:x+1] == occupiedSeat {
			// fmt.Printf("down occupied\n")
			return openSeat
		}

		if downValid && rightValid && input[down][right:right+1] == occupiedSeat {
			// fmt.Printf("down right occupied\n")
			return openSeat
		}

		return occupiedSeat
	case occupiedSeat:
		var numOccupied int

		// check all 8 directions for occupied
		if upValid && leftValid && input[up][left:left+1] == occupiedSeat {
			numOccupied++
		}

		if upValid && input[up][x:x+1] == occupiedSeat {
			numOccupied++
		}

		if upValid && rightValid && input[up][right:right+1] == occupiedSeat {
			numOccupied++
		}

		if leftValid && input[y][left:left+1] == occupiedSeat {
			numOccupied++
		}

		if rightValid && input[y][right:right+1] == occupiedSeat {
			numOccupied++
		}

		if downValid && leftValid && input[down][left:left+1] == occupiedSeat {
			numOccupied++
		}

		if downValid && input[down][x:x+1] == occupiedSeat {
			numOccupied++
		}

		if downValid && rightValid && input[down][right:right+1] == occupiedSeat {
			numOccupied++
		}

		if numOccupied >= 4 {
			return openSeat
		}
		return occupiedSeat
	}

	return "X"
}
